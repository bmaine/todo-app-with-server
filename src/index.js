const indexRouter= require('./routes/index')
const userRouter = require('./routes/users')
const express = require("express");
const expressSession = require("express-session");
const cookieParser = require ('cookie-parser')
const cors = require("cors");
const bodyParser = require("body-parser");
const logger = require("morgan");
require('./db/db')
require('dotenv').config()
const morgan = require('morgan')


const app = express();
const port = process.env.PORT || 5000;
app.use(cors())
app.use(bodyParser.json());
app.use(logger('dev'));
app.use('/', indexRouter);
app.use('/users', userRouter);

console.log('env is ', process.env)
app.listen(port, () => {
    console.log('app is running on port',port )
})