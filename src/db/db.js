const mongoose = require('mongoose')
require('dotenv').config()

console.log('env in db',process.env.MONGODB_URL)
mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
})