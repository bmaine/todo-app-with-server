const express = require('express')
const User = require('../models/User')
const { check, validationResult } = require('express-validator');

const router = express.Router()


router.post('/signup', async (req, res) => {
    try {
        // Create a new user
        check('username').isEmail(),
        // password must be at least 5 chars long
        check('password').isLength({ min: 5 })
        const user = new User(req.body)
        await user.save()
        console.log('user save')
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }
})

router.post('/login', async(req, res) => {
    //Login a registered user
    try {
        const { email, password } = req.body
        // username must be an email
        check('username').isEmail(),
        // password must be at least 5 chars long
        check('password').isLength({ min: 5 })
        const user = await User.findByCredentials(email, password)

        if (!user) {
            console.log('err inn login')
            return res.status(401).send({error: 'Login failed! Check authentication credentials'})
        }
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (error) {
        res.status(400).send(error)
    }

})

module.exports = router