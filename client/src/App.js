import React from 'react';
import './App.css';
import Topbar from './components/Topbar'
import Todo from './components/Todos'
import Gallery from './components/Gallery'

const  App =() => 
    <div className="App">
      <Topbar/>
      <Todo>
        <Todo.Title/>
        <Todo.Text/>
        <Todo.Button/>
        <Todo.TodoCompletedButton/>
      </Todo>
      <Gallery/>
    </div>

export default App;
