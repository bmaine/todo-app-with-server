import produce from 'immer'
import shortid from 'shortid'

const initialState = {
    todos: [],

}

const rootReducer = produce((draft = initialState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
        draft.todos = [
            ...draft.todos,
            {
                id: shortid.generate(),
                text: action.text,
                title: action.title,
                completed: false
            }
        ]
        return 

        case 'COMPLETE_TODO':
        draft.todos.reduce((result, value, key) => {
            if(value.id === action.id) {
                value.completed = !value.completed
            }
            return value
        }, [])
        return draft

        case 'SET_SHOW_COMPLETED':
        draft.showCompleted = !draft.showCompleted
        return draft

        default:
            return draft
    }
})

export default rootReducer