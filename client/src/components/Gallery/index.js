import React from 'react';
import GalleryWrapper from './GalleryWrapper'
import Card from '../Card'
import { filter, map } from 'lodash'
import { useDispatch, useSelector } from 'react-redux'

const  Gallery = () => {
  const todos = useSelector(state => state.todos)
  const showCompleted = useSelector(state => state.showCompleted)
  const dispatch = useDispatch()

  return (
    <GalleryWrapper >
      Gallery
      {map(filter(todos, todo => {
        return showCompleted ? todo : !todo.completed
      }), todo => (
        <Card key={todo.id} id={todo.id} todo={todo} dispatch={dispatch} />
      ))}
    </GalleryWrapper>
  );
  
}

export default Gallery;
