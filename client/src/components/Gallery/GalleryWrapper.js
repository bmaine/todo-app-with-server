
import styled from 'styled-components'

const GalleryWrapper = styled.div`
  margin: 22px;
  padding: 26px;
  border: 1px solid black;
  display: flex;
  width: 100%;
  max-width: 700px;
  flex-wrap: wrap;
`
export default GalleryWrapper;
