import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
import { filter } from 'lodash'
const Topbar = props => {
    const todos = useSelector(state => state.todos)
    const numberOfCompletedTodos = useCallback(filter(todos, ( todo ) => {
        return todo.completed
    }).length)


    return (
        <div> {numberOfCompletedTodos} completed ouf of  {todos.length} </div>
    )
}

export default Topbar