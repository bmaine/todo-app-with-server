import React, {  useContext, useMemo } from 'react'
import { TitleContext } from './context'
import TitleWrapper from './TitleWrapper'
import { Input, FormLabel } from '@material-ui/core'

const TodoTitle = props => {

    const  { handleChangeTitle, title, } = useContext(TitleContext)

    const value = useMemo(() => title, [title])
    const handleTitleChange = (e) => {
        
        handleChangeTitle(e.target.value)
    }
    
    return (
       
            <TitleWrapper>
                <FormLabel style={{margin: '8px'}}>Title</FormLabel>
                <Input onChange={handleTitleChange} value={value}/>
            
            </TitleWrapper>
       
    )
}

export { TitleContext, TodoTitle}