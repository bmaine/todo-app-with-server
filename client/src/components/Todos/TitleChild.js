import React, { createContext, useContext, useEffect, useState } from 'react'
import styled from 'styled-components'
import { connectableObservableDescriptor } from '../../../../../Library/Caches/typescript/3.3/node_modules/rxjs/internal/observable/ConnectableObservable';
import { TitleContext } from './context'
let value = '' 

const TodoTitle = props => {

  const title = useContext(TitleContext)
  
    return (
        <div value={title}>
            <div>Title child
                {title}
            </div>
        </div>
    )
}

export default TodoTitle