import styled from 'styled-components'

const TitleWrapper = styled.div`
        margin: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
`

export default TitleWrapper