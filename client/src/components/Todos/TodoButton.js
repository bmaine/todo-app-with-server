import React, { useContext } from 'react'
import {TextContext, TitleContext} from './context'
import {  useDispatch } from 'react-redux'
import { Button } from '@material-ui/core'

const TodoButton = () => {

    const { handleChangeTitle, title} = useContext(TitleContext)
    const { handleChangeText, text, disabled} = useContext(TextContext)
    const dispatch = useDispatch()
    
    const handleAddTodo = async () => {
        await dispatch({
            type: 'ADD_TODO',
            text,
            title, 
        })
        handleChangeTitle('')
        handleChangeText('')
    }
    
    return (
        <div>
            <Button  onClick={handleAddTodo} disabled={disabled}>Add Todo</Button>
        </div>
    )
}

export default TodoButton