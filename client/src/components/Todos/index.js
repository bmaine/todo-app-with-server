import React, {  useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { TodoText } from './TodoText'
import TodoButton from './TodoButton'
import TodoCompletedButton from './TodoCompletedButton'
import {  TodoTitle } from './TodoTitle'
import { TextContext, TitleContext, ShowCompletedContext} from './context'


const Todo = ({children, onToggle, onChangeTitle}) => {
    const dispatch = useDispatch()
    const [text, setText] = useState('')
    const [showCompleted, setShowCompleted] = useState(false)
    const [title, setTitle] = useState('')
    const [disabled, setDisabled] = useState(true)

    useEffect(() => {
        if(disabled && (text && title)) {
            setDisabled(false)
        }
        if(!disabled && (!text || !title)) {
            setDisabled(true)
        }
    }, [text, title])

    const handleChangeTitle = (title) => {
        setTitle(title)
    }

    const handleChangeText = (text) => {
        setText(text)
    }

    const handleChangeShowCompleted = () => {
        setShowCompleted(!showCompleted)
        dispatch({
            type: 'SET_SHOW_COMPLETED',
        })
        
    }

    return (
        <ShowCompletedContext.Provider value={{ handleChangeShowCompleted, showCompleted }}>
            <TitleContext.Provider value={{ handleChangeTitle, title }}>
                <TextContext.Provider value={{ handleChangeText, text, disabled }}>
                    <div style={{margin: '10px', border:'1px solid'}}>
                            {children}
                    </div>
                </TextContext.Provider>
            </TitleContext.Provider>
        </ShowCompletedContext.Provider>

    )
}

Todo.Title = TodoTitle
Todo.Text = TodoText
Todo.Button = TodoButton
Todo.TodoCompletedButton = TodoCompletedButton


export default Todo