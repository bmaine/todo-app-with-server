import { createContext } from 'react'

const TitleContext = createContext()
const TextContext = createContext()
const ShowCompletedContext = createContext()


export { TextContext, TitleContext, ShowCompletedContext } 