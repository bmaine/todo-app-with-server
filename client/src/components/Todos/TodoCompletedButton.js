import React, { useContext } from 'react'
import { ShowCompletedContext } from './context'
import {  FormControlLabel, FormLabel, Switch } from '@material-ui/core'

const TodoCompletedButton = () => {

    const { handleChangeShowCompleted, showCompleted} = useContext(ShowCompletedContext)
    
    const handleChangeCompleted = () => {
        handleChangeShowCompleted()
    }
    
    return (
        <div>
            <FormLabel>
                <FormControlLabel
                    control={<Switch size="small" onClick={handleChangeCompleted} checked={showCompleted}/>}
                    label="Show Completed"
                />
            </FormLabel>
        </div>
    )
}

export default TodoCompletedButton