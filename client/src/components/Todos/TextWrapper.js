import styled from 'styled-components'

const TextWrapper = styled.div`
        margin: 14px;
        display: flex;
        justify-content: center;
        align-items: center;
`

export default TextWrapper