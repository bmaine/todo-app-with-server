import React, {  useContext } from 'react'
import { TextContext } from './context'
import { TextareaAutosize, FormLabel } from '@material-ui/core'
import TextWrapper from './TextWrapper'
const TodoText = props => {

    const  { handleChangeText, ...context} = useContext(TextContext)

    const handleTextChange = (e) => {
        handleChangeText(e.target.value)
    }
    return (
       
            <TextWrapper>
                <FormLabel style={{margin: '8px'}}>Text</FormLabel>
                <TextareaAutosize  onChange={handleTextChange} value={context.text}/>
                
            
            </TextWrapper>
       
    )
}

export { TextContext, TodoText}