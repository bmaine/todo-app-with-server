import React from 'react';
import CardWrapper from './CardWrapper'
import { Checkbox } from '@material-ui/core'
const  Card = ({ dispatch, todo }) => {

  const handleComplete = () => {
    dispatch({
      type: 'COMPLETE_TODO',
      id: todo.id
    })
  }
  return (
    <CardWrapper key={todo.id} >
      <h2> {todo.title}</h2>
      <p>{todo.text}</p>
      
      <div>
        <Checkbox
          value="Completed"
          checked={todo.completed}
          onClick={handleComplete}
          inputProps={{ 'aria-label': 'Checkbox A' }}
        />
      </div>
      
    </CardWrapper>
  );
  
}

export default Card;
