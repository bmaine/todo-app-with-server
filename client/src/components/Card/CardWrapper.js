
import styled from 'styled-components'

const CardWrapper = styled.div`
  margin: 22px;
  padding: 26px;
  border: 1px solid black;
  min-height: 60px;
  min-width: 80px;
  max-width: 140px;
  align-items: center;
  justify-content: center;
`
export default CardWrapper;
